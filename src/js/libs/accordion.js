const accordionBlock = {
    init(key) {
        const clickButton = document.querySelectorAll('[data-step]');
        clickButton.forEach((item) => {
            item.addEventListener('click', (e) => {
                e.preventDefault();
                item.parentNode.toggleAttribute('data-vis');
            });
        });

        const rangeInputs = document.querySelectorAll('input[type="range"]')
        function handleInputChange(e) {
            let target = e.target
            if (e.target.type !== 'range') {
                target = document.getElementById('range')
            }
            const min = target.min
            const max = target.max
            const val = target.value

            target.style.backgroundSize = (val - min) * 100 / (max - min) + '% 100%'
        }

        rangeInputs.forEach(input => {
            input.addEventListener('input', handleInputChange)
        })
        const introHello = document.querySelectorAll('#intro-hello .container-ds');
        introHello.forEach((item) => {
            if(!localStorage.getItem('id')){
                setTimeout(() => {
                    item.style.visibility = 'visible'
                    localStorage.setItem('id', '1');
                }, 3000);
            }else if(localStorage.getItem('id')){
                const rrt = document.querySelector('#intro-hello svg');
                rrt.remove();
                item.style.visibility = 'visible';
            }
        });
    }
};

export default accordionBlock;