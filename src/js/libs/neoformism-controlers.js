const btnControlers = {
    init() {
        const elemControlers = document.querySelectorAll('[data-click]');
        elemControlers.forEach((item) => {
            item.addEventListener('click', (e) => {
                e.preventDefault();
                item.parentNode.toggleAttribute('data-show');
            });
        });
    },
};

export default btnControlers;