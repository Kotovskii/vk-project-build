import slideSlick from './slick';
import accordionBlock from "./accordion";
import btnControlers from "./neoformism-controlers";
import gameArkanoid from "./game-arkanoid";
document.addEventListener('DOMContentLoaded', () => { // когда будет готов весь DOM
    slideSlick.init();
    accordionBlock.init();
    btnControlers.init();
    gameArkanoid.init();
});