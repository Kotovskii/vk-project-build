import SlideSlick from 'slick-carousel';

const slideSlick = {
    init() {
        $(function () {

            $('.catalog__sliders').slick({
                infinite: true,
                centerMode: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                adaptiveHeight: false,
                easing: 'ease',
                pauseOnFocus: true,
                pauseOnHover: true,
                pauseOnDotsHover: true,
                draggable: true,
                swipe: true,
                rows: 0,
                variableWidth: true,
                focusOnSelect: true,
                swipeToSlide: true,
                waitForAnimate: true,
                centerPadding: '0',
                arrows: false,
                dots: false,
                autoplay: false,
            });
        });
    },
};

export default slideSlick;
